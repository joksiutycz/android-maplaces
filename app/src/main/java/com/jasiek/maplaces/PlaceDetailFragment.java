package com.jasiek.maplaces;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.query.Query;

public class PlaceDetailFragment extends Fragment {

    PlaceDetailFragmentInteractions interactions;

    TextView descriptionTextView;
    ImageButton imageButton;
    Button commentButton;

    Place place;
    Bitmap bitmap;

    OpinionsListAdapter adapter;
    RecyclerView recyclerView;
    List<Opinion> opinions;

    Box<Opinion> opinionBox;
    Box<Place> placeBox;

    public static final String ARG_ITEM_ID = "place_id";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        opinionBox = (LoginActivity.getBoxStore()).boxFor(Opinion.class);

        if(getArguments().containsKey(ARG_ITEM_ID))
        {
            placeBox = (LoginActivity.getBoxStore()).boxFor(Place.class);
            place = placeBox.find(Place_.id, getArguments().getLong(ARG_ITEM_ID, 0)).get(0);
        }

        setInteractions((PlaceDetailFragmentInteractions)getActivity());

    }


    public interface PlaceDetailFragmentInteractions {
        void addOpinion(long placeId);

    }

    private void setInteractions(PlaceDetailFragmentInteractions interactions) {
        this.interactions = interactions;
    }

    public static PlaceDetailFragment newInstance(PlaceDetailFragmentInteractions interactions) {
        PlaceDetailFragment fragment = new PlaceDetailFragment();
        fragment.setInteractions(interactions);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_place_detail, container, false);
        findViews(rootView, container);
        return rootView;
    }

    private void findViews(View rootView, ViewGroup container) {

        imageButton = rootView.findViewById(R.id.fragment_place_detail_image);
        descriptionTextView = rootView.findViewById(R.id.fragment_place_detail_description);
        commentButton = rootView.findViewById(R.id.fragment_place_detail_add_opinion);
        recyclerView = rootView.findViewById(R.id.fragment_place_detail_opinions);
        recyclerView.setLayoutManager(new LinearLayoutManager(container.getContext()));

        if(place.user.getTargetId() == (LoginActivity.getCurrentUser().id))
        {
            commentButton.setVisibility(Button.INVISIBLE);
        }

        if(place.image != null)
        {
            bitmap = BitmapFactory.decodeFile(place.image);
            if(bitmap != null)
            {
                bitmap = MainActivity.resize(bitmap, 400, 400);
                imageButton.setImageBitmap(bitmap);
            }
        }

        String desc = getResources().getString(R.string.no_description);

        Opinion opinion = opinionBox.query().equal(Opinion_.placeId, place.id )
                .equal(Opinion_.userId, place.user.getTargetId())
                .build().findFirst();

        if(opinion != null)
            desc = opinion.text;

        descriptionTextView.setText(desc);

        opinions = new ArrayList<>();
        opinions = opinionBox.query()
                .equal(Opinion_.placeId, getArguments().getLong(ARG_ITEM_ID, 3) )
                .notEqual(Opinion_.userId, opinion.user.getTargetId()).build().find();
        adapter = new OpinionsListAdapter(opinions);
        recyclerView.setAdapter(adapter);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), PhotoViewActivity.class);
                intent.putExtra("image" , place.image);
                startActivity(intent);
            }
        });

        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interactions.addOpinion(place.id);
            }
        });

    }
}

