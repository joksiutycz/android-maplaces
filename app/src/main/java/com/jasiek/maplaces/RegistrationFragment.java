package com.jasiek.maplaces;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.objectbox.Box;

/**
 * Created by jasiek on 18.01.18.
 */

public class RegistrationFragment extends Fragment {

    RegistrationFragmentInteractions interactions;

    EditText userLoginEditText;
    EditText passwordEditText;
    EditText confirmPasswordEditText;
    Button signinButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setInteractions(RegistrationFragmentInteractions interactions) {
        this.interactions = interactions;
    }

    public interface RegistrationFragmentInteractions {
        void signinUser(String name, String password, String confirmPassword);
    }

    public static RegistrationFragment newInstance(RegistrationFragmentInteractions interactions) {
        RegistrationFragment fragment = new RegistrationFragment();
        fragment.setInteractions(interactions);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_registration, container, false);
        findViews(rootView);
        return rootView;
    }

    private void findViews(View rootView){

        userLoginEditText = rootView.findViewById(R.id.fragment_registration_userlogin);
        passwordEditText = rootView.findViewById(R.id.fragment_registration_password);
        confirmPasswordEditText = rootView.findViewById(R.id.fragment_registration_password_confirm);
        signinButton = rootView.findViewById(R.id.fragment_registration_signin_button);


        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String login = userLoginEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                String confirmPassword = confirmPasswordEditText.getText().toString();

                interactions.signinUser(login, password, confirmPassword);

            }
        });
    }
}
