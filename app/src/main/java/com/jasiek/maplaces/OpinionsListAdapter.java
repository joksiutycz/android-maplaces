package com.jasiek.maplaces;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;



public class OpinionsListAdapter extends RecyclerView.Adapter<OpinionsListAdapter.ViewHolder> {

    private List<Opinion> data;
    private Context context;


    public OpinionsListAdapter(List<Opinion> data){
        this.data = data;
    }

    @Override
    public OpinionsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.opinion_list_item, parent, false);
        context = parent.getContext();

        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(data.get(position));

    }

    public void setData(List<Opinion> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView userName;
        private TextView opinionText;


        public ViewHolder(final View itemView) {
            super(itemView);

            userName = itemView.findViewById(R.id.opinion_list_item_username);
            opinionText = itemView.findViewById(R.id.opinion_list_item_opinion);

        }

        public void bindData(Opinion opinion){
            opinionText.setText(opinion.text);
            userName.setText(context.getResources().getString(R.string.written_by, opinion.user.getTarget().login));
        }
    }
}