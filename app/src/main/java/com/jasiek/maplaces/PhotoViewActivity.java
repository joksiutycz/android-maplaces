package com.jasiek.maplaces;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;


public class PhotoViewActivity extends Activity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);

        Intent intent = getIntent();
        String image = intent.getStringExtra("image");

        Bitmap bitmap = BitmapFactory.decodeFile(image);

        imageView = findViewById(R.id.activity_photo_view_image);
        imageView.setImageBitmap(bitmap);
    }
}
