package com.jasiek.maplaces;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.objectbox.Box;


public class AddOpinionFragment extends Fragment {

    public static final String ARG_ITEM_ID = "place_id";

    AddOpinionFragmentInteractions interactions;

    private EditText opinionEditText;
    private Button submit;


    public static AddOpinionFragment newInstance(AddOpinionFragmentInteractions interactions) {
        AddOpinionFragment fragment = new AddOpinionFragment();
        fragment.setInteractions(interactions);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setInteractions(AddOpinionFragmentInteractions interactions) {
        this.interactions = interactions;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_opinion, container, false);
        findViews(rootView);
        return rootView;
    }

    private void findViews(View rootView) {
        submit = rootView.findViewById(R.id.fragment_add_opinion_button_add);
        opinionEditText = rootView.findViewById(R.id.fragment_add_opinion_text);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(opinionEditText.getText().length() == 0)
                {
                    Toast.makeText(getContext(), getResources().getString(R.string.opinion_cant_be_empty), Toast.LENGTH_SHORT).show();
                    return;
                }

                Opinion opinion = new Opinion();
                opinion.text = opinionEditText.getText().toString();
                opinion.user.setTarget((LoginActivity.getCurrentUser()));

                Box<Opinion> opinionBox = (LoginActivity.getBoxStore()).boxFor(Opinion.class);

                opinion.place.setTargetId(getArguments().getLong(ARG_ITEM_ID, 0));
                opinionBox.put(opinion);


                interactions.updateOpinions(getArguments().getLong(ARG_ITEM_ID, 0));
            }
        });
    }


    public interface AddOpinionFragmentInteractions {
        void updateOpinions(long placeId);
    }
}
