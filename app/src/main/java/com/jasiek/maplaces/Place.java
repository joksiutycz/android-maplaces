package com.jasiek.maplaces;


import java.util.Date;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;
import io.objectbox.relation.ToOne;

/**
 * Created by jasiek on 18.01.18.
 */
@Entity
public class Place {
    @Id
    public long id;

    public String image;

    public ToOne<User> user;

    @Backlink(to = "place")
    public ToMany<Opinion> opinions;

    public double latitude;
    public double longitude;

}
