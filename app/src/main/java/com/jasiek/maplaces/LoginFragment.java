package com.jasiek.maplaces;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by jasiek on 18.01.18.
 */

public class LoginFragment extends Fragment {

    LoginFragmentInteractions interactions;

    EditText userLoginEditText;
    EditText passwordEditText;
    Button loginButton;
    Button signinButton;

    public interface LoginFragmentInteractions {
        void loginUser(String login, String password);
        void loginUser();
        void signinUser();
    }

    public void setInteractions(LoginFragmentInteractions interactions) {
        this.interactions = interactions;
    }

    public static LoginFragment newInstance(LoginFragmentInteractions interactions) {
        LoginFragment fragment = new LoginFragment();
        fragment.setInteractions(interactions);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setInteractions((LoginFragmentInteractions) getActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        findViews(rootView);
        return rootView;
    }

    private void findViews(View rootView){

        userLoginEditText = rootView.findViewById(R.id.fragment_login_userlogin);
        passwordEditText = rootView.findViewById(R.id.fragment_login_password);
        loginButton = rootView.findViewById(R.id.fragment_login_login_button);
        signinButton = rootView.findViewById(R.id.fragment_login_signin_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String login = userLoginEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                interactions.loginUser(login, password);

                passwordEditText.setText("");
                userLoginEditText.setText("");
            }
        });

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                interactions.signinUser();
            }
        });
    }


}
