package com.jasiek.maplaces;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

/**
 * Created by jasiek on 18.01.18.
 */

@Entity
public class Opinion {
    @Id
    public long id;

    public ToOne<User> user;

    public ToOne<Place> place;

    public String text;
}
