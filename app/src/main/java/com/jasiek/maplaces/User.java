package com.jasiek.maplaces;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;

/**
 * Created by jasiek on 18.01.18.
 */

@Entity
public class User {
    @Id
    public long id;

    public String login;
    public String password;


    @Backlink(to = "user")
    public ToMany<Place> places;

    @Backlink(to = "user")
    public ToMany<Opinion> opinions;

    public User (){

    }

    public User(String login, String password){
        this.login = login;
        this.password = password;

    }
}
