package com.jasiek.maplaces;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;
import io.objectbox.query.QueryBuilder;

public class LoginActivity extends AppCompatActivity implements RegistrationFragment.RegistrationFragmentInteractions, LoginFragment.LoginFragmentInteractions {

    private static BoxStore boxStore;

    private static User currentUser;

    private final static int MY_PERMISSIONS_REQUEST_LOCATION = 0;
    private final static int MY_PERMISSIONS_REQUEST_INTERNET = 1;
    private final static int MY_PERMISSIONS_REQUEST_STORAGE = 2;
    private final static int MY_PERMISSIONS_REQUEST_CAMERA = 3;

    private LoginFragment loginFragment;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_INTERNET: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                return;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }

        if(boxStore == null)
            boxStore = MyObjectBox.builder().androidContext(LoginActivity.this).build();

        if(savedInstanceState != null)
        {
            return;
        }


        loginFragment = LoginFragment.newInstance(this);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_login_fragment_container, loginFragment, "LoginFragment")
                .commit();

    }

    public static BoxStore getBoxStore() {
        return boxStore;
    }

    public static User getCurrentUser(){
        return currentUser;
    }

    public static void logout(){
        currentUser = null;
    }

    @Override
    public void loginUser(String login, String password) {
        System.out.println("loginUser");
        Box<User> usersBox = (LoginActivity.getBoxStore().boxFor(User.class));
        Query<User> query = usersBox.query().equal(User_.login, login).
                equal(User_.password, password, QueryBuilder.StringOrder.CASE_SENSITIVE).build();
        currentUser = query.findFirst();
        if(currentUser == null)
        {
            Toast.makeText(getApplicationContext(), R.string.login_failed, Toast.LENGTH_SHORT).show();
            return;
        }

        Toast.makeText(getApplicationContext(), R.string.login_success, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    @Override
    public void loginUser()
    {

    }

    @Override
    public void signinUser() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(R.id.activity_login_fragment_container, RegistrationFragment.newInstance(this), "RegistrationFragment")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void signinUser(String login, String password, String confirmPassword){

        if(!password.equals(confirmPassword))
        {
            Toast.makeText(LoginActivity.this, R.string.password_not_match, Toast.LENGTH_SHORT).show();
            return;
        }

        Box<User> usersBox = (LoginActivity.getBoxStore().boxFor(User.class));
        Query<User> query = usersBox.query().equal(User_.login, login).
                equal(User_.password, password, QueryBuilder.StringOrder.CASE_SENSITIVE).build();

        if(query.count() > 0)
        {
            Toast.makeText(LoginActivity.this, R.string.login_taken, Toast.LENGTH_SHORT).show();
            return;
        }

        User user = new User(login, password);
        usersBox.put(user);

        loginUser(user.login, user.password);

    }
}
