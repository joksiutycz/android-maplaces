package com.jasiek.maplaces;


/**
 * Created by jasiek on 01.12.17.
 */


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;


import java.util.Date;

import io.objectbox.Box;


public class AddPlaceFragment extends Fragment {

    AddPlaceFragmentInteractions interactions;

    private ImageView imageView;
    private EditText description;
    private Button submit;
    private Button takePicture;

    Bitmap thumb = null;

    public static AddPlaceFragment newInstance(AddPlaceFragmentInteractions interactions) {
        AddPlaceFragment fragment = new AddPlaceFragment();
        fragment.setInteractions(interactions);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setInteractions(AddPlaceFragmentInteractions interactions) {
        this.interactions = interactions;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_place, container, false);
        findViews(rootView);
        return rootView;
    }

    private void findViews(View rootView) {
        description = rootView.findViewById(R.id.fragment_add_place_description);
        submit = rootView.findViewById(R.id.fragment_add_place_button_add);
        takePicture = rootView.findViewById(R.id.fragment_add_place_button_photo);
        imageView = rootView.findViewById(R.id.fragment_add_place_thumbnail);

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                interactions.addPhoto();

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(description.getText().length() == 0)
                {
                    Toast.makeText(getContext(), getResources().getString(R.string.description_cant_be_empty), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!interactions.checkIfPhotoTaken())
                {
                    Toast.makeText(getContext(), getResources().getString(R.string.you_have_to_add_photo), Toast.LENGTH_SHORT).show();
                    return;
                }


                Place place = new Place();
                place.user.setTarget(LoginActivity.getCurrentUser());
                place.latitude = getArguments().getDouble("lat");
                place.longitude = getArguments().getDouble("lng");


                Opinion opinion = new Opinion();
                opinion.text = description.getText().toString();
                opinion.user.setTarget((LoginActivity.getCurrentUser()));

                Box<Opinion> opinionBox = (LoginActivity.getBoxStore()).boxFor(Opinion.class);
                opinion.place.setTarget(place);
                opinionBox.put(opinion);

                interactions.addPlace(place);
            }
            });
    }

    public void updatePhoto(){
        Bitmap thumb = BitmapFactory.decodeFile(interactions.getPhotoPath());
        if(thumb != null)
        {
            thumb = (MainActivity.resize(thumb, 400, 400));
            imageView.setImageBitmap(thumb);
        }
    }

    public interface AddPlaceFragmentInteractions {
        void addPlace(Place place);
        void addPhoto();
        boolean checkIfPhotoTaken();
        String getPhotoPath();
    }

}