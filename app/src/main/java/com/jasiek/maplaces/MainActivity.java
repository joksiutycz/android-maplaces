package com.jasiek.maplaces;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.objectbox.Box;

public class MainActivity extends AppCompatActivity implements
        PlaceDetailFragment.PlaceDetailFragmentInteractions,
        AddPlaceFragment.AddPlaceFragmentInteractions,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        OnMapReadyCallback,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener,
        AddOpinionFragment.AddOpinionFragmentInteractions {

    private final static int MY_PERMISSIONS_REQUEST_LOCATION = 0;
    private final static int MY_PERMISSIONS_REQUEST_INTERNET = 1;
    private final static int MY_PERMISSIONS_REQUEST_STORAGE = 2;
    private final static int MY_PERMISSIONS_REQUEST_CAMERA = 3;

    public final static int MAX_THUMBNAIL_WIDTH = 480;
    public final static int MAX_THUMBNAIL_HEIGHT = 320;

    private final static String MAP_ZOOM_LEVEL = "zoom";
    private static float currentZoom;

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private final static String MAP_FILTER = "filter";
    private int currentFilter;

    public String mCurrentPhotoPath;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;


    GoogleMap map;
    LatLng myPosition;

    Menu menu;

    List<Marker> markers;

    SupportMapFragment mapFragment;

    Snackbar mySnackbar;

    @Override
    public void updateOpinions(long placeId) {

//        Bundle args = new Bundle();
//        args.putLong(PlaceDetailFragment.ARG_ITEM_ID, placeId);

        FragmentManager fragmentManager = getSupportFragmentManager();
//        PlaceDetailFragment placeDetailFragment = PlaceDetailFragment.newInstance(this);
//        placeDetailFragment.setArguments(args);

        View mView = findViewById(R.id.activity_main_fragment_details);
        mView.setVisibility(View.VISIBLE);

//        fragmentManager
//                .beginTransaction()
//                .replace(R.id.activity_main_fragment_details, fragmentManager.findFragmentByTag("PlaceDetailFragment"), "PlaceDetailFragment")
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                .commit();

        Toast.makeText(this, R.string.opinion_added, Toast.LENGTH_SHORT).show();
        fragmentManager.popBackStack();

    }

    @Override
    public void addOpinion(long placeId) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        AddOpinionFragment addOpinionFragment = AddOpinionFragment.newInstance(this);
        Bundle args = new Bundle();
        args.putLong(AddOpinionFragment.ARG_ITEM_ID, placeId);
        addOpinionFragment.setArguments(args);

        findViewById(R.id.activity_main_fragment_details).setVisibility(View.VISIBLE);

        fragmentManager.beginTransaction()
                .addToBackStack("PlaceDetailFragment")
                .replace(R.id.activity_main_fragment_details, addOpinionFragment, "AddOpinionFragment")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.map_marker_info_window, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            ImageView ivImage = myContentsView.findViewById(R.id.image);
            TextView tvSnippet = myContentsView.findViewById(R.id.snippet);
            TextView tvTitle = myContentsView.findViewById(R.id.title);

            String image = null;
            Bitmap bitmap = null;

            if ((long) marker.getTag() != (long) -1) {
                Box<Place> placeBox = (LoginActivity.getBoxStore()).boxFor(Place.class);
                Place place = placeBox.query().equal(Place_.id, (long) marker.getTag()).build().findFirst();
                if (place != null && place.image != null)
                    image = place.image;
                bitmap = BitmapFactory.decodeFile(image);
            }

            if (image == null)
                ivImage.setImageResource(R.mipmap.ic_launcher_round);
            else if (bitmap != null) {
                bitmap = resize(bitmap, MAX_THUMBNAIL_WIDTH, MAX_THUMBNAIL_HEIGHT);
                ivImage.setImageBitmap(bitmap);
            }

            tvTitle.setText(marker.getTitle());
            tvSnippet.setText(marker.getSnippet());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }
    }

    boolean doubleBackToExitPressedOnce = false;


    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = fm.findFragmentById(R.id.activity_main_fragment_details);

        if(fm.getBackStackEntryCount() > 0){
            fm.popBackStack();
        } else if(doubleBackToExitPressedOnce){
            finish();
        } else if(!doubleBackToExitPressedOnce)
        {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getResources().getString(R.string.press_back_again_to_exit), Toast.LENGTH_SHORT).show();
        }


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public class MyUndoListener implements View.OnClickListener {

        public Marker marker;

        public MyUndoListener(Marker marker) {
            this.marker = marker;
        }

        @Override
        public void onClick(View v) {

            marker.remove();
            markers.remove(marker);
            mySnackbar.dismiss();
            findViewById(R.id.activity_main_fragment_details).setVisibility(View.GONE);
            menu.findItem(R.id.options_menu_add_place).setEnabled(true);

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_INTERNET: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(2000);
        mLocationRequest.setFastestInterval(1500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.activity_main_fragment_map);
        mapFragment.getMapAsync(this);

        findViewById(R.id.activity_main_fragment_details).setVisibility(View.GONE);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            myPosition = new LatLng(location.getLatitude(), location.getLongitude());
                            map.moveCamera(CameraUpdateFactory.newLatLng(myPosition));

                        }
                    }
                });


        if (savedInstanceState != null) {
            currentZoom = savedInstanceState.getFloat(MAP_ZOOM_LEVEL);
            currentFilter = savedInstanceState.getInt(MAP_FILTER);
            return;
        }
        createLocationRequest();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    myPosition = new LatLng(location.getLatitude(), location.getLongitude());
                    System.out.println(myPosition.latitude + " " + myPosition.longitude);
                }
            }
        };

        currentZoom = 15;

        currentFilter = R.id.options_menu_show_all_places;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putFloat(MAP_ZOOM_LEVEL, map.getCameraPosition().zoom);
        outState.putInt("filter", currentFilter);

        super.onSaveInstanceState(outState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        this.menu = menu;
        inflater.inflate(R.menu.options_menu, menu);
        menu.findItem(currentFilter).setChecked(true);
        return true;
    }

    public void onGroupItemClick(MenuItem item) {
        item.setChecked(true);
        int id = item.getItemId();
        filterMarkers(id);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.options_menu_add_place:
                startAddingPlace(myPosition);
                return true;
            case R.id.options_menu_logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void logout(){
        LoginActivity.logout();
        NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }

        markers = new ArrayList<>();

        map.setInfoWindowAdapter(new MyInfoWindowAdapter());
        map.setOnMarkerClickListener(this);
        map.setOnMapLongClickListener(this);
        map.setOnMapClickListener(this);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setMyLocationEnabled(true);
        map.setOnMyLocationButtonClickListener(this);
        map.setOnMyLocationClickListener(this);
        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        map.moveCamera(CameraUpdateFactory.zoomTo(currentZoom));

        Box<Place> placeBox = (LoginActivity.getBoxStore()).boxFor(Place.class);
        List<Place> places = placeBox.getAll();
        for (int i = 0; i < places.size(); i++) {
            addMarker(places.get(i));
        }

        filterMarkers(currentFilter);

    }

    public static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    public Marker addMarker(Place place) {
//        Box<Opinion> opinionBox = (LoginActivity.getBoxStore()).boxFor(Opinion.class);
//        Opinion opinion = opinionBox.query().equal(Opinion_.placeId, place.id)
//                .equal(Opinion_.userId, place.user.getTargetId())
//                .build().findFirst();


        String title = getResources().getString(R.string.added_by_user, place.user.getTarget().login);
        //String desc = getResources().getString(R.string.no_description);
//        if (opinion != null)
//            desc = opinion.text;

        LatLng newPlace = new LatLng(place.latitude, place.longitude);

        Marker marker = map.addMarker(new MarkerOptions()
                .position(newPlace)
                .title(title));
        marker.setTag(place.id);

        if(place.user.getTarget().id == (LoginActivity.getCurrentUser().id))
        {
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            marker.setTitle(getResources().getString(R.string.added_by_current_user));
        }

        markers.add(marker);

        return marker;
    }

    public void filterMarkers(int filter)
    {
        Box<Place> placeBox = (LoginActivity.getBoxStore()).boxFor(Place.class);
        long userId = LoginActivity.getCurrentUser().id;
        List<Place> myPlaces = placeBox.query().equal(Place_.userId, userId).build().find();
        ArrayList<Long> ids = new ArrayList<>();
        for(int i = 0; i < myPlaces.size(); i++)
        {
            ids.add(myPlaces.get(i).id);
        }
        switch (filter){
            case R.id.options_menu_show_all_places:
                for(int i = 0; i < markers.size(); i++)
                {
                    markers.get(i).setVisible(true);
                }
                break;

            case R.id.options_menu_show_my_places_only:
                for(int i = 0; i < markers.size(); i++)
                {
                    markers.get(i).setVisible(false);
                }

                for(int i = 0; i < ids.size(); i++)
                {
                    for(int j = 0; j < markers.size(); j++)
                    {
                        if(ids.get(i) == (long)markers.get(j).getTag())
                        {
                            markers.get(j).setVisible(true);
                        }
                    }
                }
                break;

            case R.id.options_menu_show_others_places_only:
                for(int i = 0; i < markers.size(); i++)
                {
                    markers.get(i).setVisible(true);
                }

                for(int i = 0; i < ids.size(); i++)
                {
                    for(int j = 0; j < markers.size(); j++)
                    {
                        if(ids.get(i) == (long)markers.get(j).getTag())
                            markers.get(j).setVisible(false);
                    }
                }
                break;
        }
        currentFilter = filter;
    }

    @Override
    public void addPlace(Place place) {
        Box<Place> placeBox = (LoginActivity.getBoxStore()).boxFor(Place.class);
        place.image = mCurrentPhotoPath;
        placeBox.put(place);

        Bundle args = new Bundle();
        args.putLong(PlaceDetailFragment.ARG_ITEM_ID, place.id);
        deleteNotAddedMarkers();

        addMarker(place).showInfoWindow();

        FragmentManager fragmentManager = getSupportFragmentManager();
        PlaceDetailFragment placeDetailFragment = PlaceDetailFragment.newInstance(this);
        placeDetailFragment.setArguments(args);

        fragmentManager
                .beginTransaction()
                .replace(R.id.activity_main_fragment_details, placeDetailFragment, "PlaceDetailFragment")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

        if(currentFilter == R.id.options_menu_show_others_places_only)
            filterMarkers(R.id.options_menu_show_all_places);

        menu.findItem(R.id.options_menu_add_place).setEnabled(true);
        Toast.makeText(this, getResources().getString(R.string.place_added), Toast.LENGTH_SHORT).show();
    }

    public void startAddingPlace(LatLng latLng){

        menu.findItem(R.id.options_menu_add_place).setEnabled(false);

        mCurrentPhotoPath = null;

        deleteNotAddedMarkers();

        Marker marker = map.addMarker(new MarkerOptions()
                .position(latLng)
                .title(getResources().getString(R.string.new_place)));
        marker.setTag((long)-1);
        marker.showInfoWindow();
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

        markers.add(marker);

        FragmentManager fragmentManager = getSupportFragmentManager();

        AddPlaceFragment addPlaceFragment = AddPlaceFragment.newInstance(this);
        Bundle args = new Bundle();
        args.putDouble("lat", latLng.latitude);
        args.putDouble("lng", latLng.longitude);
        addPlaceFragment.setArguments(args);

        findViewById(R.id.activity_main_fragment_details).setVisibility(View.VISIBLE);

        fragmentManager.beginTransaction()
                .replace(R.id.activity_main_fragment_details, addPlaceFragment, "AddPlaceFragment")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

        mySnackbar = Snackbar.make(findViewById(R.id.activity_main_fragment_details)
                , getResources().getString(R.string.new_place), Snackbar.LENGTH_INDEFINITE);
        mySnackbar.setAction(getResources().getString(R.string.cancel), new MyUndoListener(marker));
        mySnackbar.show();

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }

    @Override
    public void addPhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    public boolean checkIfPhotoTaken() {
        if(mCurrentPhotoPath == null)
            return false;
        else
            return true;
    }

    @Override
    public String getPhotoPath() {
        return mCurrentPhotoPath;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Toast.makeText(this, mCurrentPhotoPath, Toast.LENGTH_LONG).show();
        return image;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE)
        {
            if( resultCode == RESULT_OK) {
                ((AddPlaceFragment)getSupportFragmentManager().findFragmentByTag("AddPlaceFragment"))
                        .updatePhoto();
            }
            else if(resultCode == RESULT_CANCELED)
            {
                mCurrentPhotoPath = null;
            }
        }

    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        //Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        //Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();

        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        findViewById(R.id.activity_main_fragment_details).setVisibility(View.GONE);
        deleteNotAddedMarkers();
    }


    @Override
    public void onMapLongClick(LatLng latLng) {
        startAddingPlace(latLng);
    }

    public void deleteNotAddedMarkers()
    {
        if(mySnackbar != null && mySnackbar.isShown())
        {
            for (int i = 0; i < markers.size(); i++) {
                if ((long) markers.get(i).getTag() == (long)-1) {
                    markers.get(i).remove();
                    markers.remove(i);
                }
            }
            mySnackbar.dismiss();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        deleteNotAddedMarkers();

        if ((long) marker.getTag() == -1)
            return false;

        Bundle args = new Bundle();
        args.putLong(PlaceDetailFragment.ARG_ITEM_ID, (long) marker.getTag());

        FragmentManager fragmentManager = getSupportFragmentManager();
        PlaceDetailFragment placeDetailFragment = PlaceDetailFragment.newInstance(this);
        placeDetailFragment.setArguments(args);

        View mView = findViewById(R.id.activity_main_fragment_details);
        mView.setVisibility(View.VISIBLE);

        fragmentManager
                .beginTransaction()
                .replace(R.id.activity_main_fragment_details, placeDetailFragment, "PlaceDetailFragment")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();

        return false;
    }

}
